describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));

    it("fills all the text input fiels", () => {
        const firstName = "Fernando";
        const lastName = "Eduardo";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("fernandinho.eduardo@gmail.com");
        cy.get("#request").type("Vegetarin");
        cy.get("#first-name").type("Fernando");
        cy.get("#signature").type('${firstName} ${lastName}');
    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });

    it("selects 'social midia' checkbox", () => {
        cy.get("#social-media").check();
    });

    it("selects 'friend', and 'publication', then uncheck 'frined'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", () => {
        cy.get("#email")
        .as("email")
        .type("fernandinho.eduardo-gmail.com");

        cy.get("#email.invalid").should("exist");

       cy.get("@email")
         .clear()
         .type("fernandinho.eduardo@gmail.com"); 

        cy.get("@invalidEmail").should("not.exist");
    });
    
    it.only("fills and reset the formn", () => {
        const firstName = "Fernando";
        const lastName = "Eduardo";
        const fullName = '${firstName} ${lastName}';

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("fernandinho.eduardo@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#request").type("VegetarIPA beer");

        cy.get(".agreement p").should(
            "contain",
            'I, ${fullName}, wish to buy 2 VIP tickets.'
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullname);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });
});